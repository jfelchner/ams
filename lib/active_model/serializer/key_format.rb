module ActiveModel
  class Serializer
    module KeyFormat
      extend ActiveSupport::Concern

      included do |base|
        class << base
          attr_accessor :_key_format
        end

        def key_format
          options[:key_format] || self.class.key_format
        end
      end

      module ClassMethods
        def key_format(format = nil)
          format ? @_key_format = format : (@_key_format || config.key_format)
        end
      end
    end
  end
end
