require 'test_helper'

module ActiveModel
  class Serializer
    class KeyFormatTest < Minitest::Test
      def setup
        @author = Author.new(name: 'Steve K.')

        @first_post = Post.new(
                        id:                   1,
                        title:                'Hello!!',
                        body:                 'Hello, world!!',
                        multi_word_attribute: 'Something about hello')
        @second_post = Post.new(
                        id:                   2,
                        title:                'New Post',
                        body:                 'Body',
                        multi_word_attribute: 'Something about new')

        @author.posts = [@first_post, @second_post]

        @first_post.author = @author
        @second_post.author = @author

        @first_post.comments = []
        @second_post.comments = []
      end

      def test_overrides_default_adapter_key_format
        @serializer = PostWithKeyFormatSerializer.new(@first_post)
        @adapter = ActiveModel::Serializer::Adapter::JsonApi.new(@serializer)

        assert @adapter.serializable_hash[:data][:attributes].key?(:'multi_word_attribute')
        assert @adapter.serializable_hash[:data][:relationships].key?(:'tons_of_comments')
        assert @adapter.serializable_hash[:data][:relationships].key?(:'stoic_author')
      end

      def test_key_format_can_be_passed_at_instance_level
        @serializer = PostWithMultiWordKeysSerializer.new(@first_post, key_format: :underscore)
        @adapter = ActiveModel::Serializer::Adapter::JsonApi.new(@serializer)

        assert @adapter.serializable_hash[:data][:attributes].key?(:'multi_word_attribute')
        assert @adapter.serializable_hash[:data][:relationships].key?(:'tons_of_comments')
        assert @adapter.serializable_hash[:data][:relationships].key?(:'stoic_author')
      end
    end
  end
end
